# hardhat VeChain Thor plugin

* hardhat-thor allows to deploy and interact with contracts through hardhat with VeChain

## Installation

```shell
yarn add @vechain.energy/hardhat-thor
```

Add the following line to `hardhat.config.js`:

```js
require('@vechain.energy/hardhat-thor')
```

## Configuration

VeChain network is be configured in `hardhat.config.js` file:

```js
module.exports = {
  solidity: "0.8.4",
  networks: {
    vechain: {
      url: 'https://testnet.veblocks.net',
      privateKey: "0x…",
      delegateUrl: 'https://sponsor-testnet.vechain.energy/by/#',
      blockGasLimit: 10000000
    }
  }
};
```

Each network configuration accepts 
- `url`: used to connect to the desired network node
- `privateKey`: private key used for deployment and interaction with the blockchain
- `accounts[]`: (optional) list of private keys to use as signer for blockchain interaction
- `delegateUrl`: (optional) makes use of fee delegation and let another user pay for gas fees

Default values are:

```js
{
  url: 'https://testnet.veblocks.net',
  privateKey: '0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80'
}
```

- Defaults to network `vechain` which can be changed:
  - in the command line with `--network <network name>`
  - with an environment variable `NETWORK=<network name>`

## Usage

The plugin exposes `hre.thor` for interaction with the VeChain-Thor

### Deployment

`hre.thor` works as `hre.ethers` replacement:

```js
const Greeter = await hre.thor.getContractFactory("Greeter");
const greeter = await Greeter.deploy("Hello, VeChain!");
```

### Interaction

```js
const Greeter = await hre.thor.getContractFactory("Greeter");
const greeter = await Greeter.deploy("Hello, VeChain!");

await greeter.deployed();

console.log("Greeter deployed to:", greeter.address);

const deployedGreeter = await hre.thor.getContractAt('Greeter', greeter.address)
const greeting = await deployedGreeter.greet()
console.log("Greeter responded with:", greeting)
 ```
