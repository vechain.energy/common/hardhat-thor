const thor = require('@vechain/web3-providers-connex')
const getConnexProvider = require('./getConnexProvider')
const collectLibrariesAndLink = require('./modules/collectLibrariesAndLink')

const DEFAULT_NETWORK = 'vechain'

module.exports = function ThorProxy (hre) {
  let provider

  const networkName = !process.argv.includes('--network')
    ? process.env.NETWORK || DEFAULT_NETWORK
    : process.argv[process.argv.indexOf('--network') + 1]

  async function ensureProviderIsInitialized () {
    if (!provider) {
      if (!hre.config.networks[networkName]) {
        throw new Error(`network "${networkName}" not found`)
      }

      hre.network.name = networkName
      hre.network.config = hre.config.networks[hre.network.name]
      hre.network.provider = hre.ethers.provider = provider = await getConnexProvider(hre.network.config)
    }
  }

  async function resolveSigner (signer) {
    if (!signer) {
      return (await getSigners())[0]
    }

    if (typeof signer === 'string') {
      return getSigner(signer)
    }

    return signer
  }

  const getSigner = async (address) => {
    await ensureProviderIsInitialized()
    return provider.getSigner(address)
  }
  const getSigners = async () => {
    await ensureProviderIsInitialized()
    const accounts = await provider.listAccounts()
    return accounts.map(address => provider.getSigner(address))
  }

  const getContractFactoryFromArtifact = async (contractName, signerOrOptions, signerOrUndefined) => {
    const artifact = await hre.artifacts.readArtifact(contractName)
    const libraries = signerOrOptions?.libraries ? signerOrOptions.libraries : { }
    const signer = signerOrUndefined || signerOrOptions?.signer || (signerOrOptions?.libraries ? undefined : signerOrOptions)

    const linkedBytecode = await collectLibrariesAndLink(artifact, libraries)
    return getContractFactoryByAbiAndBytecode(artifact.abi, linkedBytecode, signer)
  }

  const getContractFactoryByAbiAndBytecode = async (abi, bytecode, signer) => {
    const wallet = await resolveSigner(signer)
    return thor.ethers.modifyFactory(
      new hre.ethers.ContractFactory(abi, bytecode, wallet)
    )
  }

  return {
    getSigner,
    getSigners,

    getContractAt: async (...args) => {
      await ensureProviderIsInitialized()
      return hre.ethers.getContractAt(...args)
    },
    getContractFactory: async (nameOrAbi, bytecodeOrFactoryOptions, signer) => {
      await ensureProviderIsInitialized()

      if (typeof nameOrAbi === 'string') {
        return getContractFactoryFromArtifact(nameOrAbi, bytecodeOrFactoryOptions, signer)
      }

      return getContractFactoryByAbiAndBytecode(nameOrAbi, bytecodeOrFactoryOptions, signer)
    },

    getProvider: async () => {
      await ensureProviderIsInitialized()
      return provider
    },
    verifyContract: (address, name, args, customArgs) => { throw new Error('@vechain.energy/hardhat-thor verifyContract() not implemented') }
  }
}
