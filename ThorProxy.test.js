const ThorProxy = require('./ThorProxy')

const mockProvider = {
  getSigner: jest.fn(),
  listAccounts: jest.fn(() => [])
}
const getConnexProvider = require('./getConnexProvider')
jest.mock('./getConnexProvider', () => jest.fn(() => mockProvider))

beforeEach(() => {
  process.argv = [...process.argv.slice(0, 2)]
  delete process.env.NETWORK
})

describe('ThorProxy', () => {
  it('is a function', async () => {
    expect(ThorProxy).toBeInstanceOf(Function)
  })

  it('returns an object', async () => {
    const Proxy = ThorProxy()
    expect(Proxy).toBeInstanceOf(Object)
  })

  describe('network', () => {
    it('supports --network <network name> to select a configured network', async () => {
      const networkName = 'cli-test'
      process.argv = [...process.argv.slice(0, 2), '--network', networkName]
      const networkConfig = { random: Math.random() }
      const proxy = ThorProxy(getHre({ networkConfig, networkName }))
      await proxy.getProvider()
      expect(getConnexProvider).toBeCalledWith(networkConfig)
      process.argv = [...process.argv.slice(0, 2)]
    })

    it('supports env NETWORK=<network name> to select a configured network', async () => {
      const networkName = 'env-test'
      process.env.NETWORK = networkName
      process.argv = [...process.argv.slice(0, 2)]
      const networkConfig = { random: Math.random() }
      const proxy = ThorProxy(getHre({ networkConfig, networkName }))
      await proxy.getProvider()
      expect(getConnexProvider).toBeCalledWith(networkConfig)
      delete process.env.NETWORK
    })

    it('switches hardhat network to the active network', async () => {
      process.argv = [...process.argv.slice(0, 2), '--network', 'test']
      const networkConfig = { random: Math.random() }
      const networkName = 'test'
      const hre = getHre({ networkConfig, networkName })
      const proxy = ThorProxy(hre)
      const provider = await proxy.getProvider()
      expect(hre.network.name).toEqual(networkName)
      expect(hre.network.config).toEqual(networkConfig)
      expect(hre.network.provider).toEqual(provider)
    })

    it('rejects invalid networks', async () => {
      process.argv = [...process.argv.slice(0, 2), '--network', 'invalid']
      const networkConfig = { random: Math.random() }
      const networkName = 'valid'
      const proxy = ThorProxy(getHre({ networkConfig, networkName }))
      await expect(proxy.getProvider).rejects.toThrow('network "invalid" not found')
    })

    it('defaults to "vechain" as network', async () => {
      process.argv = [...process.argv.slice(0, 2)]
      const networkConfig = { random: Math.random() }
      const proxy = ThorProxy(getHre({ networkConfig, networkName: 'vechain' }))
      await proxy.getProvider()
      expect(getConnexProvider).toBeCalledWith(networkConfig)
    })

    it('passes the "config.networks[selectedNetwork]" to getConnexProvider()', async () => {
      process.argv = [...process.argv.slice(0, 2)]
      const networkConfig = { random: Math.random() }
      const proxy = ThorProxy(getHre({ networkConfig, networkName: 'vechain' }))
      await proxy.getProvider()
      expect(getConnexProvider).toBeCalledWith(networkConfig)
    })
  })

  it.each`
  name | type
  ${'getSigner'} | ${Function}
  ${'getSigners'} | ${Function}
  ${'getContractAt'} | ${Function}
  ${'getContractFactory'} | ${Function}
  ${'getProvider'} | ${Function}  
  `('has attribute "$name" of type "$type"', async ({ name, type }) => {
    const Proxy = ThorProxy()
    expect(Proxy[name]).toBeInstanceOf(type)
  })

  describe('getSigner(address)', () => {
    it('ensures that a provider is initialized', async () => {
      const networkConfig = { random: Math.random() }
      const proxy = ThorProxy(getHre({ networkConfig }))
      await proxy.getSigner('0x')
      expect(getConnexProvider).toBeCalledWith(networkConfig)
    })

    it('passes signer from provider.getSigner(address)', async () => {
      const signer = { random: Math.random() }
      const address = `0x${Math.random()}`
      const Proxy = ThorProxy(getHre())
      mockProvider.getSigner.mockReturnValue(signer)

      const result = await Proxy.getSigner(address)

      expect(mockProvider.getSigner).toHaveBeenCalledTimes(1)
      expect(mockProvider.getSigner).toHaveBeenCalledWith(address)
      expect(result).toEqual(signer)
    })
  })

  describe('getSigners()', () => {
    it('ensures that a provider is initialized', async () => {
      const networkConfig = { random: Math.random() }
      const proxy = ThorProxy(getHre({ networkConfig }))
      await proxy.getSigners()
      expect(getConnexProvider).toBeCalledWith(networkConfig)
    })

    it('returns a signer instance of each address in provider.listAccounts()', async () => {
      const addresses = [`0x${Math.random()}`, `0x${Math.random()}`]
      const signerMap = {}
      addresses.forEach(address => { signerMap[address] = { address } })

      mockProvider.listAccounts.mockReturnValue(addresses)
      mockProvider.getSigner = jest.fn((address) => signerMap[address])

      const Proxy = ThorProxy(getHre())
      const result = await Proxy.getSigners()

      expect(mockProvider.listAccounts).toHaveBeenCalledTimes(1)
      expect(mockProvider.getSigner).toHaveBeenCalledTimes(addresses.length)

      result.forEach(signer => {
        expect(signer).toEqual(signerMap[signer.address])
      })
    })
  })

  describe('getProvider()', () => {
    it('ensures that a provider is initialized', async () => {
      const networkConfig = { random: Math.random() }
      const proxy = ThorProxy(getHre({ networkConfig }))
      await proxy.getProvider()
      expect(getConnexProvider).toBeCalledWith(networkConfig)
    })

    it('returns the connex modified provider', async () => {
      const Proxy = ThorProxy(getHre())
      const provider = await Proxy.getProvider()
      expect(provider).toEqual(mockProvider)
    })
  })

  describe('getContractAt()', () => {
    it('ensures that a provider is initialized', async () => {
      const networkConfig = { random: Math.random() }
      const proxy = ThorProxy(getHre({ networkConfig }))
      await proxy.getContractAt()
      expect(getConnexProvider).toBeCalledWith(networkConfig)
    })

    it('passes all arguments to hre.ethers.getContractAt()', async () => {
      const hre = getHre()
      const proxy = ThorProxy(hre)
      const randomArgs = [Math.random(), Math.random(), Math.random()]
      await proxy.getContractAt(...randomArgs)
      expect(hre.ethers.getContractAt).toBeCalledWith(...randomArgs)
    })

    it('returns the raw result from hre.ethers.getContractAt()', async () => {
      const hre = getHre()
      const randomResult = { random: Math.random() }
      hre.ethers.getContractAt.mockReturnValue(randomResult)

      const proxy = ThorProxy(hre)

      const result = await proxy.getContractAt()
      expect(result).toEqual(randomResult)
    })
  })

  describe('getContractFactory()', () => {
    it('ensures that a provider is initialized', async () => {
      const networkConfig = { random: Math.random() }
      const proxy = ThorProxy(getHre({ networkConfig }))
      await proxy.getContractFactory()
      expect(getConnexProvider).toBeCalledWith(networkConfig)
    })

    it('supports getContractFactory(abi, bytecode)', async () => {
      const hre = getHre()
      const proxy = ThorProxy(hre)
      const abi = [Math.random()]
      const bytecode = `0x${Math.random()}`

      await proxy.getContractFactory(abi, bytecode)
      const signer = (await proxy.getSigners())[0]
      expect(hre.ethers.ContractFactory).toHaveBeenCalledWith(abi, bytecode, signer)
    })

    it('supports getContractFactory(abi, bytecode, signer)', async () => {
      const hre = getHre()
      const proxy = ThorProxy(hre)
      const abi = [Math.random()]
      const bytecode = `0x${Math.random()}`

      const signer = (await proxy.getSigners())[0]
      signer._modified = Math.random()

      await proxy.getContractFactory(abi, bytecode, signer)
      expect(hre.ethers.ContractFactory).toHaveBeenCalledWith(abi, bytecode, signer)
    })

    it('uses hre.artifacts.readArtifact to load contract data by name', async () => {
      const hre = getHre()
      const proxy = ThorProxy(hre)
      const abi = [Math.random()]
      const bytecode = `0x${Math.random()}`
      const contractName = `Contract_${Math.random()}`

      hre.artifacts.readArtifact.mockReturnValue({ abi, bytecode })

      await proxy.getContractFactory(contractName)

      expect(hre.artifacts.readArtifact).toBeCalledWith(contractName)
    })

    it('supports getContractFactory(contractName)', async () => {
      const hre = getHre()
      const proxy = ThorProxy(hre)
      const abi = [Math.random()]
      const bytecode = `0x${Math.random()}`

      hre.artifacts.readArtifact.mockReturnValue({ abi, bytecode })

      await proxy.getContractFactory('test')
      const signer = (await proxy.getSigners())[0]
      expect(hre.ethers.ContractFactory).toHaveBeenCalledWith(abi, bytecode, signer)
    })

    it('supports getContractFactory(contractName, signer)', async () => {
      const hre = getHre()
      const proxy = ThorProxy(hre)
      const abi = [Math.random()]
      const bytecode = `0x${Math.random()}`

      hre.artifacts.readArtifact.mockReturnValue({ abi, bytecode })

      const signer = (await proxy.getSigners())[1]
      signer._modified = Math.random()

      await proxy.getContractFactory('test', signer)
      expect(hre.ethers.ContractFactory).toHaveBeenCalledWith(abi, bytecode, signer)
    })

    it('supports getContractFactory(contractName, factoryOptions) with libraries', async () => {
      const hre = getHre()
      const proxy = ThorProxy(hre)
      const abi = [Math.random()]
      const bytecode = `0x${Math.random()}`
      const linkReferences = { 'contracts/libraries/LibExample1.sol': { LibExample1: [{ length: 20, start: 133 }] }, 'contracts/libraries/LibExample2.sol': { LibExample2: [{ length: 20, start: 133 }] } }
      const libAddress1 = '0xf9F07298FFeC57788882416D146Cf1163aB076FA'
      const libAddress2 = '0xf9F07298FFeC57788882416D146Cf1163aB076FA'
      hre.artifacts.readArtifact.mockReturnValue({ abi, bytecode, linkReferences })

      await proxy.getContractFactory('test', { libraries: { LibExample1: libAddress1, LibExample2: libAddress2 } })
      const signer = (await proxy.getSigners())[0]
      expect(hre.ethers.ContractFactory).toHaveBeenCalledWith(abi, `${bytecode}${libAddress1.slice(2)}${libAddress2.slice(2)}`, signer)
    })

    it('supports getContractFactory(contractName, factoryOptions, signer) with libraries', async () => {
      const hre = getHre()
      const proxy = ThorProxy(hre)
      const abi = [Math.random()]
      const bytecode = `0x${Math.random()}`
      const linkReferences = { 'contracts/libraries/LibExample1.sol': { LibExample1: [{ length: 20, start: 133 }] }, 'contracts/libraries/LibExample2.sol': { LibExample2: [{ length: 20, start: 133 }] } }
      const libAddress1 = '0xf9F07298FFeC57788882416D146Cf1163aB076FA'
      const libAddress2 = '0xf9F07298FFeC57788882416D146Cf1163aB076FA'
      hre.artifacts.readArtifact.mockReturnValue({ abi, bytecode, linkReferences })

      const signer = (await proxy.getSigners())[1]
      signer._modified = Math.random()

      await proxy.getContractFactory('test', { libraries: { LibExample1: libAddress1, LibExample2: libAddress2 } }, signer)
      expect(hre.ethers.ContractFactory).toHaveBeenCalledWith(abi, `${bytecode}${libAddress1.slice(2)}${libAddress2.slice(2)}`, signer)
    })
  })
})

function getHre ({ networkConfig, networkName = 'vechain', defaultNetwork } = {}) {
  return (
    {
      network: {},
      config: {
        defaultNetwork,
        networks: {
          [networkName]: { ...networkConfig }
        }
      },
      ethers: {
        getContractAt: jest.fn(),
        getContractFactory: jest.fn(),
        ContractFactory: jest.fn()
      },
      artifacts: {
        readArtifact: jest.fn()
      }
    }
  )
}
