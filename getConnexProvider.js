const { Framework } = require('@vechain/connex-framework')
const { Driver, SimpleNet, SimpleWallet } = require('@vechain/connex-driver')
const { ethers } = require('ethers')
const thor = require('@vechain/web3-providers-connex')

const DEFAULT_NETWORK_URL = 'https://testnet02.vechain.de.blockorder.net'
const DEFAULT_PRIVATE_KEY = '0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80'

module.exports = async function getConnexProvider ({ url = DEFAULT_NETWORK_URL, privateKey, accounts = [], delegateUrl, ...config } = {}) {
  const net = new SimpleNet(url)
  const wallet = new SimpleWallet()
  const signerAccounts = Array.isArray(accounts) ? [...accounts] : []

  if (!privateKey && !signerAccounts.length) {
    signerAccounts.push(DEFAULT_PRIVATE_KEY)
  } else if (privateKey) {
    signerAccounts.unshift(privateKey)
  }

  signerAccounts.forEach(privateKey => wallet.import(privateKey))

  const driver = await Driver.connect(net, wallet)
  const connex = new Framework(driver)

  const connexArgs = { connex, wallet }
  if (delegateUrl) {
    connexArgs.delegate = { url: delegateUrl }
  }

  const provider = thor.ethers.modifyProvider(
    new ethers.providers.Web3Provider(
      new thor.Provider(connexArgs)
    )
  )

  return provider
}
