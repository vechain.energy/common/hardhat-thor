const { ethers } = require('ethers')

const getConnexProvider = require('./getConnexProvider')

const thor = require('@vechain/web3-providers-connex')
jest.mock('@vechain/web3-providers-connex', () => {
  const originalModule = jest.requireActual('@vechain/web3-providers-connex')

  return {
    __esModule: true,
    ...originalModule,
    Provider: jest.fn((...args) => new originalModule.Provider(...args))
  }
})

jest.setTimeout(30000)

describe('getConnexProvider', () => {
  it('is a function', async () => {
    expect(getConnexProvider).toBeInstanceOf(Function)
  })

  it('returns a web3-provider', async () => {
    const provider = await getConnexProvider()
    expect(provider).toBeInstanceOf(ethers.providers.Web3Provider)
  })

  it('respects the given "privateKey"', async () => {
    const { address, privateKey } = ethers.Wallet.createRandom()
    const provider = await getConnexProvider({ privateKey })
    expect(provider.provider.wallet.keys[0].address.toLowerCase()).toEqual(address.toLowerCase())
  })

  describe('accounts', () => {
    it('supports accounts configuration"', async () => {
      const accounts = [...new Array(2)].map(() => ethers.Wallet.createRandom().privateKey)
      const provider = await getConnexProvider({ accounts })
      expect(provider.provider.wallet.keys.length).toEqual(accounts.length)
      provider.provider.wallet.keys.forEach((key, index) => {
        expect(key.address.toLowerCase()).toEqual((new ethers.Wallet(accounts[index])).address.toLowerCase())
      })
    })

    it('prioritizes privateKey as accounts[0]', async () => {
      const { address, privateKey } = ethers.Wallet.createRandom()
      const accounts = [...new Array(2)].map(() => ethers.Wallet.createRandom().privateKey)
      const provider = await getConnexProvider({ accounts, privateKey })

      expect(provider.provider.wallet.keys[0].address.toLowerCase()).toEqual(address.toLowerCase())
      expect(provider.provider.wallet.keys.length).toEqual(accounts.length + 1)
      provider.provider.wallet.keys.slice(1).forEach((key, index) => {
        expect(key.address.toLowerCase()).toEqual((new ethers.Wallet(accounts[index])).address.toLowerCase())
      })
    })

    it('can handle accounts as non-list-input"', async () => {
      const { address, privateKey } = ethers.Wallet.createRandom()
      const provider = await getConnexProvider({ accounts: 'remote', privateKey })
      expect(provider.provider.wallet.keys.length).toEqual(1)
      expect(provider.provider.wallet.keys[0].address.toLowerCase()).toEqual(address.toLowerCase())
    })
  })

  it('enables fee delegation if delegateUrl is given', async () => {
    const delegateUrl = 'https://${Math.random}'
    await getConnexProvider({ delegateUrl })

    expect(thor.Provider).toHaveBeenCalledWith(
      expect.objectContaining({ delegate: { url: delegateUrl } })
    )
  })

  it('does not enable fee delegation if delegateUrl is not given', async () => {
    await getConnexProvider()

    expect(thor.Provider).not.toHaveBeenCalledWith(
      expect.objectContaining({ delegate: {} })
    )
  })
})
