const { extendEnvironment } = require('hardhat/config')
const ThorProxy = require('./ThorProxy')

extendEnvironment((hre) => {
  hre.thor = ThorProxy(hre)
})
